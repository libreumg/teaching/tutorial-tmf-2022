---
title: "DQ-Report GMDS"
author: "Stephan Struckmann"
date: "2022-08-25"
output:
  word_document: default
  html_document:
    df_print: paged
  pdf_document: default
---

```{r setup, echo=FALSE, warning=FALSE}
library(dataquieR, quietly = TRUE)

library(flextable, quietly = TRUE) # For Word
use_df_printer() # For Word
# set flextable defaults
knitr::opts_chunk$set(echo = FALSE)

knitr::knit_hooks$set(
   error = function(x, options) {
     paste('\n\n<div class="alert alert-danger">',
           gsub('##', '\n', gsub('^##\ Error', '**Error**', x)),
           '</div>', sep = '\n')
   },
   warning = function(x, options) {
     paste('\n\n<div class="alert alert-warning">',
           gsub('##', '\n', gsub('^##\ Warning:', '**Warning**', x)),
           '</div>', sep = '\n')
   },
   message = function(x, options) {
     paste('\n\n<div class="alert alert-info">',
           gsub('##', '\n', x),
           '</div>', sep = '\n')
   }
)

set_flextable_defaults(
  font.family = "Arial",
  font.size = 8,
  theme_fun = "theme_vanilla",
  big.mark = "",
  table.layout = "autofit"
)

# sd1 <- readRDS(system.file("extdata", "ship.RDS", package = "dataquieR"))
# md1 <- readRDS(system.file("extdata", "ship_meta.RDS", package = "dataquieR"))
sd1 <- rio::import("../../data/ship.RDS")
md1 <- rio::import("../../data/ship_meta.RDS")
```

# Metadata Overview

```{r}
cols <- names(WELL_KNOWN_META_VARIABLE_NAMES)
cols <- intersect(colnames(md1), cols)
cols <- setdiff(cols, VALUE_LABELS)
autofit(flextable(md1[, head(cols, 10)]), hspans = "divided")
```
```{r}
autofit(flextable(md1[, c(cols[[1]], tail(cols, 9))]), hspans = "divided")
```

# Blutdruck

```{r warning=!FALSE}
l <- acc_loess(
  study_data = sd1, 
  meta_data = md1,
  resp_vars = "SBP_0.2",
  label_col = LABEL,
  group_vars = "OBS_BP_0",
  time_vars = "EXAM_DT_0",
  co_vars = c("SEX_0", "AGE_0"))
l$SummaryPlot$SBP_0.2
```

# Appendix
