# TMF Tutorial „Wie implementiere ich Datenqualitätsassessments?

Dr. Stephan Struckmann, Dr. Joany Marino, Elisa Kasbohm, Prof. Dr. Carsten Oliver Schmidt  

Berlin/Online, 15.11.2022  

_Work in progress_

## Beschreibung 

Das Tutorial besteht aus drei Teilen. Erstens einer Einführung in ein Datenqualitätskonzept für Forschungsdaten (Schmidt et al. 2021). Vorgestellt und besprochen werden Dimensionen der Datenqualität und deren Umsetzung in Datenqualitätsindikatoren. Zweitens werden Voraussetzungen für Metadaten besprochen, um umfassende Datenqualitätsassessments zu ermöglichen. Im dritten und längsten Block werden diese Grundlagen dann übertragen auf die Erstellung eigener Datenqualitätsreports mit Fokus auf das R dataquieR package (Richter et al. 2021).  Neben der Nutzung von Demo-Daten besteht  die Option, eigene Daten mitzubringen und auf diesen Reports zu erstellen.   

Mehr Info auf unser [Web](https://dataquality.ship-med.uni-greifswald.de/).

### Mitzubringende Arbeitsmaterialien der Teilnehmer
* Laptop (Intel CPU, at least 4 GByte of RAM and 4 cores), 
* R >= 4.1 und R-Studio installiert, 
* Rtools (im Falle von Windows) installiert; 
* Folgende R-Pakete werden verwendet und sollten installiert sein: dataquieR, rio. 
* Idealerweise:
    - dieses Gitlab-Repository geklont.
    - eigener Datensatz mit 10 bis 40 Variablen zur Erstellung eines individuellen Datenqualitätsberichts